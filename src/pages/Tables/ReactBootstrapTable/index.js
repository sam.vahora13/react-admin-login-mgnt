import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import generateData from '../generateData';

const data = generateData(1000);
const axios = require('axios')

class ReactBootstrapTable extends Component {

  // generateData(500, false)
  state = {
    data: []
  };

  componentDidMount() {
    axios.get('/getAllUser')
        .then((response) => {
          // handle success
          this.setState({
            data:response.data
          })
          console.log(response);
        })
        .catch((error) => {
          // handle error
          console.log(error);
        })

  }

  removeItem = itemId => {
    this.setState({
      data: data.filter(item => item.id !== itemId)
    });
  }

  render() {
    const { data } = this.state;
    const options = {
      sizePerPage: 20,
      prePage: 'Previous',
      nextPage: 'Next',
      firstPage: 'First',
      lastPage: 'Last',
      hideSizePerPage: true,
    };

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="header">
                <h4>Firabase User List</h4>
                {/*<p>React Bootstrap Table is a multi-features, powerful data table for React. Check it at here: <a href="http://allenfang.github.io/react-bootstrap-table/index.html" target="_blank">http://allenfang.github.io/react-bootstrap-table</a></p>*/}
              </div>
              <div className="content">
                <BootstrapTable
                  data={data}
                  bordered={false}
                  striped
                  pagination={true}
                  options={options}>
                  <TableHeaderColumn
                    dataField='id'
                    isKey
                    width="200px"
                    dataSort>
                    UID
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='email'
                    width="15%"
                    filter={ { type: 'TextFilter'} }
                    dataSort>
                    Email
                  </TableHeaderColumn>
                  <TableHeaderColumn
                    dataField='providerId'
                    dataFormat={imageFormatter}
                    width="15%"
                    dataSort>
                    Provider Id
                  </TableHeaderColumn>
                  {/*<TableHeaderColumn*/}
                  {/*  dataField='salary'*/}
                  {/*  width="15%"*/}
                  {/*  dataSort>*/}
                  {/*  Salary*/}
                  {/*</TableHeaderColumn>*/}
                  {/*<TableHeaderColumn*/}
                  {/*  dataField='job'*/}
                  {/*  width="15%">*/}
                  {/*  Job*/}
                  {/*</TableHeaderColumn>*/}
                  {/*<TableHeaderColumn*/}
                  {/*  dataField='description'*/}
                  {/*  width="30%">*/}
                  {/*  Description*/}
                  {/*</TableHeaderColumn>*/}
                  <TableHeaderColumn width="20%"></TableHeaderColumn>
                </BootstrapTable>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}
export default ReactBootstrapTable

const getProviderName = (providerId) => {

  let providerImgUrl = "";

  switch(providerId){
    case "google.com" : providerImgUrl = "/public/users/googleIcon.png";
      break;
    case "facebook.com" : providerImgUrl = "/public/users/facebookIcon.png";
      break;
    default             :providerImgUrl = "/public/users/emailIcon.png";
      break;
  }

  return providerImgUrl;
}

const imageFormatter = (cell, row) => {

  return "<img src=user-type/"+cell+".svg>" ;
}